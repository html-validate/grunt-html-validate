# grunt-html-validate

> Validate files with `html-validate`

## Install

```
$ npm install --save-dev grunt-html-validate
```

## Usage

```js
require("load-grunt-tasks")(grunt);

grunt.initConfig({
  htmlvalidate: {
    default: {
      src: ["file.html"],
    },
  },
});
```

## Options

### `format`

Type: `string|string[]|[string, object][]`<br>
Default: `stylish`

Set the output formatter.

```js
{
  options: {
    format: ["stylish", ["json", { dest: "report.json" }]];
  }
}
```

### `failOnError`

Type: `boolean`<br>
Default: true

Fail the build if any errors are encountered.
