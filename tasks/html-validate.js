const { CLI } = require("html-validate");

/**
 * Takes formatter data in any of the following formats:
 *
 * - string
 * - string[]
 * - [string, object][]
 *
 * The option object can contain the following options:
 *
 * - "dest": set output destination file
 */
function prepareFormatter(formatter) {
	if (!Array.isArray(formatter)) {
		formatter = [formatter];
	}

	return formatter
		.map((entry) => (Array.isArray(entry) ? entry : [entry])) // string -> string[]
		.map((entry) => (entry.length < 2 ? [entry[0], {}] : entry)) // [string] -> [string, object]
		.map(([name, options]) => {
			if (options.dest) {
				return `${name}=${options.dest}`;
			} else {
				return name;
			}
		})
		.join(",");
}

/**
 * @param {CLI} cli
 */
function getFormatter(cli, options) {
	const formatString = prepareFormatter(options.format);
	return cli.getFormatter(formatString);
}

module.exports = function (grunt) {
	grunt.registerMultiTask("htmlvalidate", "Validate HTML files", async function () {
		const done = this.async();

		try {
			const cli = new CLI();
			const htmlvalidate = await cli.getValidator();
			const options = this.options({
				failOnError: true,
				format: "stylish",
			});
			const formatter = await getFormatter(cli, options);

			const filenames = this.files.map((it) => it.src).flat();
			const report = await htmlvalidate.validateMultipleFiles(filenames);
			const output = formatter(report);
			if (output) {
				grunt.log.write(output);
			}

			done(report.valid || !options.failOnError);
		} catch (err) {
			done(err);
		}
	});
};
