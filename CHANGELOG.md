# grunt-html-validate changelog

## 4.0.0 (2024-12-23)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v18 or later
- **deps:** require html-validate v8 or later

### Features

- **deps:** require html-validate v8 or later ([443a3f3](https://gitlab.com/html-validate/grunt-html-validate/commit/443a3f3039a0dd2bb62c94df8171ed9ee084ef26))
- **deps:** require nodejs v18 or later ([2b5d06d](https://gitlab.com/html-validate/grunt-html-validate/commit/2b5d06d8cbec67ee0b585a4c39e0724370459d97))
- **deps:** support html-validate v9 ([98cd566](https://gitlab.com/html-validate/grunt-html-validate/commit/98cd566ed5b6a7c04610d7e14e999623d0767446))

## [3.0.0](https://gitlab.com/html-validate/grunt-html-validate/compare/v2.0.0...v3.0.0) (2023-06-04)

### ⚠ BREAKING CHANGES

- **deps:** require nodejs v16 or later
- **deps:** require html-validate v5 or later

### Features

- **deps:** require html-validate v5 or later ([1cb74a4](https://gitlab.com/html-validate/grunt-html-validate/commit/1cb74a49ecf2b6c6b4a88450071067801e39f52e))
- **deps:** require nodejs v16 or later ([890df8b](https://gitlab.com/html-validate/grunt-html-validate/commit/890df8bfa33354de75294f29f71908a4bea7179b))

### Dependency upgrades

- **deps:** support html-validate v8 ([fe11cba](https://gitlab.com/html-validate/grunt-html-validate/commit/fe11cbaa8914d314e6396d6cda0f8efbb0b47fc6))

## [2.0.0](https://gitlab.com/html-validate/grunt-html-validate/compare/v1.0.2...v2.0.0) (2022-05-08)

### ⚠ BREAKING CHANGES

- require node 14

### Features

- require node 14 ([aa6001a](https://gitlab.com/html-validate/grunt-html-validate/commit/aa6001a285fb33246147d1555fd54cd1d63ca649))

### Dependency upgrades

- **deps:** update dependency html-validate to v7 ([d0c13d6](https://gitlab.com/html-validate/grunt-html-validate/commit/d0c13d66170d677e4d4f3e20cd9f8be7c2e014c5))

### [1.0.2](https://gitlab.com/html-validate/grunt-html-validate/compare/v1.0.1...v1.0.2) (2021-09-27)

### Dependency upgrades

- **deps:** update dependency html-validate to v6 ([57249d0](https://gitlab.com/html-validate/grunt-html-validate/commit/57249d04b2890ca4388e46086c14e5207e0e5b08))

### [1.0.1](https://gitlab.com/html-validate/grunt-html-validate/compare/v1.0.0...v1.0.1) (2021-07-04)

### Dependency upgrades

- **deps:** update dependency html-validate to v5 ([ee3c19a](https://gitlab.com/html-validate/grunt-html-validate/commit/ee3c19abb49ff862e2b474c0bb519e1435b79d43))

## [1.0.0](https://gitlab.com/html-validate/grunt-html-validate/compare/v0.5.0...v1.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([e90381e](https://gitlab.com/html-validate/grunt-html-validate/commit/e90381e8ecb1b90234269ae087dbf0f92ae57b22))

## [0.5.0](https://gitlab.com/html-validate/grunt-html-validate/compare/v0.4.7...v0.5.0) (2020-11-08)

### Features

- html-validate v4 compatibility ([8aa230b](https://gitlab.com/html-validate/grunt-html-validate/commit/8aa230bf0ac5149ec2448e7bcbe4b5e7a2d7973c))
- rewrite to use `CLI` api ([a5ac5ce](https://gitlab.com/html-validate/grunt-html-validate/commit/a5ac5ce813af887249f3bdbf0d6c2612f3618deb))

## [0.4.7](https://gitlab.com/html-validate/grunt-html-validate/compare/v0.4.6...v0.4.7) (2020-11-01)

## [0.4.6](https://gitlab.com/html-validate/grunt-html-validate/compare/v0.4.5...v0.4.6) (2020-10-25)

## [0.4.5](https://gitlab.com/html-validate/grunt-html-validate/compare/v0.4.4...v0.4.5) (2020-03-29)

## 0.4.4 (2019-02-19)

- Migrate project to gitlab.com

## 0.4.3 (2018-11-07)

- Bump html-vlidate to 0.14.2
- Bump eslint to 5.8.0

## 0.4.2 (2018-10-14)

- Bump html-vlidate to 0.11.1
- Bump eslint to 5.7.0

## 0.4.1 (2018-08-11)

- Bump html-validate to 0.10.0
- Bump eslint to 5.3.0

## 0.4.0 (2018-06-17)

- Support `html-validate@0.9`.

## 0.3.0 (2018-06-13)

- Support setting output destination.
- Added `format` option to set formatters.
- Added `failOnError` option to not fail builds.
