module.exports = function (grunt) {
	grunt.task.loadTasks("./tasks");
	grunt.registerTask("default", ["htmlvalidate"]);

	grunt.initConfig({
		htmlvalidate: {
			default: {
				options: {
					failOnError: false,
					format: ["stylish", ["json", { dest: "./result.json" }]],
				},
				src: ["test/*.html"],
			},
		},
	});
};
